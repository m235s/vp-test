import keyBy from 'lodash/keyBy';
import * as ActionTypes from '../actions';

const heroes = (state = {
    isFetching:  false,
    items:       [],
    detail:      {},
    error:       false,
    loadOnMount: true,
    offset:      0,
    limit:       20,
}, action) => {
    switch (action.type) {
    case ActionTypes.REQUEST_HEROES_LIST:
        return {
            ...state,
            loadOnMount: false,
            isFetching:  true,
            error:       false,
        };
    case ActionTypes.RECEIVE_HEROES_LIST:
        return {
            ...state,
            isFetching: false,
            items:      [...state.items, ...action.results],
            total:      action.total,
            // store heroes detail separately to avoid pagination problem, duplicate...
            detail:     { ...state.detail, ...keyBy(action.results, 'id') },
            offset:     state.offset + action.count,
            remaining:  action.total - state.items.length - action.count,
        };
    case ActionTypes.FAIL_TO_FETCH:
        console.log(action);
        return {
            ...state,
            isFetching: false,
            error:      true,
            message:    action.message,
        };
    case ActionTypes.SET_CURRENT_HERO:
        return {
            ...state,
            isFetching:  false,
            currentHero: action.id,
        };
    // this actions are called only if user type an id directly in the url and hero wasn't found in prevalued detail map
    case ActionTypes.REQUEST_HERO:
        return { ...state, isFetching: true, error: false };
    case ActionTypes.RECEIVE_HERO:
        return {
            ...state,
            isFetching: false,
            detail:     {
                ...state.detail,
                [action.id]: action.hero,
            },
        };
    default:
        return state;
    }
};

export default heroes;
