import React from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

const styles = {
    centered: {
        textAlign: 'center',
    },
};

const Error = ({ message, classes }) => (
    <Grid item xs={12} className={classes.centered}>
        {message && (
            <Typography gutterBottom variant="display1" component="h2">
                {message}
            </Typography>
        )}
    </Grid>
);

Error.propTypes = {
    message: PropTypes.string,
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => {
    const { heroes } = state;
    return { message: heroes.message };
};

export default compose(
    withStyles(styles, {
        name: 'Error',
    }),
    connect(mapStateToProps),
)(Error);
