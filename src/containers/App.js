import React from 'react';
import { Provider } from 'react-redux';
import createHistory from 'history/createHashHistory'; // todo : replace by a browser history
import { Switch, Route } from 'react-router';
import { ConnectedRouter as Router } from 'react-router-redux';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import lightBlue from '@material-ui/core/colors/lightBlue';
import grey from '@material-ui/core/colors/grey';
import configureStore from '../store';
import List from './List';
import Detail from './Detail';
import Error from './Error';
import Layout from '../components/Layout';

const history = createHistory();
const store = configureStore(history);
const theme = createMuiTheme({
    palette: {
        primary: {
            main: grey[800],
        },
        secondary: {
            main: lightBlue[800],
        },
    },
});

const App = () => (
    <MuiThemeProvider theme={theme}>
        <CssBaseline />
        <Layout>
            <Provider store={store}>
                <Router history={history}>
                    <Switch>
                        <Route exact path="/" component={List} />
                        <Route path="/hero/:id" component={Detail} />
                        <Route path="/error" component={Error} />
                    </Switch>
                </Router>
            </Provider>
        </Layout>
    </MuiThemeProvider>
);

export default App;
