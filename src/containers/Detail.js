import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import { fetchHeroIfNotExist, setCurrentHero } from '../actions';
import HeroThumbnail from '../components/HeroThumbnail';
import HeroDescription from '../components/HeroDescription';
import ComicList from '../components/ComicList';

class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: props.id.toString(),
        };
    }

    componentDidMount() {
        const { dispatch, id } = this.props;
        dispatch(fetchHeroIfNotExist(id));
    }

    static getDerivedStateFromProps(props, state) {
        if (props.id !== state.id) {
            props.dispatch(setCurrentHero(props.id)); // just for information
            props.dispatch(fetchHeroIfNotExist(props.id));
            return {
                id: props.id.toString(),
            };
        }
        return {};
    }

    render() {
        const { hero, isFetching } = this.props;
        return (
            <Fragment>
                {hero && (
                    <Fragment>
                        {/* Page title */}
                        <Grid item xs={4}>
                            <HeroThumbnail name={hero.name} {...hero.thumbnail} />
                        </Grid>
                        <Grid item xs={8}>
                            <HeroDescription name={hero.name} description={hero.description} />
                            <ComicList comics={hero.comics.items} />
                        </Grid>
                    </Fragment>
                )}
                {isFetching && (
                    <CircularProgress color="secondary" />
                )}
            </Fragment>
        );
    }
}

Detail.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    hero:       PropTypes.object,
    isFetching: PropTypes.bool,
    dispatch:   PropTypes.func.isRequired,
};

const mapStateToProps = (state, ownProps) => {
    const { heroes } = state;
    const { match: { params } } = ownProps;
    return {
        ...params,
        hero:       heroes.detail[params.id],
        isFetching: heroes.isFetching,
    };
};

export default connect(mapStateToProps)(Detail);
