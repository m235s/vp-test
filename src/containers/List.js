import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import { fetchHeroesIfNeeded, showHero } from '../actions';
import HeroCard from '../components/HeroCard';

const styles = theme => ({
    title: {
        textAlign: 'center',
        padding:   theme.spacing.unit * 2,
    },
    centered: {
        textAlign: 'center',
    },
});

class List extends Component {
    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(fetchHeroesIfNeeded(true));
    }

    render() {
        const {
            heroes,
            onLoadMore,
            classes,
            onShowHero,
        } = this.props;
        return (
            <Fragment>
                {/* Page title */}
                <Grid item xs={12}>
                    <Typography gutterBottom variant="display2" component="h1" className={classes.title}>
                        Marvel Superheroes
                    </Typography>
                </Grid>
                {/* Display heroes */}
                {heroes && heroes.items && heroes.items.map(hero => (
                    <Grid key={hero.id} item xs={3}>
                        <HeroCard
                            key={hero.id}
                            onClick={onShowHero}
                            {...hero}
                        />
                    </Grid>
                ))}
                {/* Load more content */}
                <Grid item xs={12} className={classes.centered}>
                    {heroes && heroes.isFetching && (
                        <CircularProgress color="secondary" />
                    )}
                    {heroes && !heroes.isFetching && heroes.remaining > 0 && (
                        <Button
                            onClick={onLoadMore}
                            variant="contained"
                            color="secondary"
                        >
                            Load more
                        </Button>
                    )}
                </Grid>
            </Fragment>
        );
    }
}

List.propTypes = {
    heroes:     PropTypes.object,
    dispatch:   PropTypes.func.isRequired,
    onLoadMore: PropTypes.func.isRequired,
    classes:    PropTypes.object.isRequired,
};

List.defaultProps = {
    heroes: {},
};

const mapDispatchToProps = dispatch => ({
    onLoadMore: () => dispatch(fetchHeroesIfNeeded()),
    onShowHero: id => dispatch(showHero(id)),
    dispatch,
});

const mapStateToProps = (state) => {
    const { heroes } = state;
    return { heroes };
};

export default compose(
    withStyles(styles, {
        name: 'List',
    }),
    connect(mapStateToProps, mapDispatchToProps),
)(List);
