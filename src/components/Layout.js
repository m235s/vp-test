import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    grid: {
        padding: theme.spacing.unit * 4,
    },
});

const Layout = ({ children, classes }) => (
    <div className={classes.root}>
        <div className={classes.grid}>
            <Grid container spacing={24} alignItems="flex-start" justify="center" direction="row">
                {children}
            </Grid>
        </div>
    </div>
);

Layout.propTypes = {
    children: PropTypes.oneOfType([
        PropTypes.node,
        PropTypes.func,
        PropTypes.element,
    ]).isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Layout);
