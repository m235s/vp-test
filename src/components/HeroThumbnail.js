import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';

const styles = {
    row: {
        display:        'flex',
        justifyContent: 'center',
    },
    avatar: {
        margin: 10,
        width:  250,
        height: 250,
    },
};

const HeroThumbnail = ({
    name,
    path,
    extension,
    classes,
}) => (
    <div className={classes.row}>
        <Avatar
            alt={name}
            src={`${path}.${extension}`}
            className={classes.avatar}
        />
    </div>
);

HeroThumbnail.propTypes = {
    name:      PropTypes.string,
    path:      PropTypes.string.isRequired,
    extension: PropTypes.string.isRequired,
    classes:   PropTypes.object.isRequired,
};

export default withStyles(styles)(HeroThumbnail);
