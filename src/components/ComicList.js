import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({
    paper: theme.mixins.gutters({
        paddingTop:    16,
        paddingBottom: 16,
        marginTop:     theme.spacing.unit * 2,
    }),
});

const ComicList = ({ classes, comics }) => {
    if (comics.length === 0) {
        return null;
    }
    return (
        <Fragment>
            <Typography gutterBottom variant="display1" component="h2">
                Comics
            </Typography>
            <Paper className={classes.paper} elevation={2}>
                <List component="nav">
                    {comics.map(comic => (
                        <ListItem key={comic.resourceURI} button>
                            <ListItemText primary={comic.name} />
                        </ListItem>
                    ))}
                </List>
            </Paper>
        </Fragment>
    );
};

ComicList.propTypes = {
    comics:  PropTypes.array.isRequired,
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ComicList);
