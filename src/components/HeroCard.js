import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import ButtonBase from '@material-ui/core/ButtonBase';
import Typography from '@material-ui/core/Typography';

const styles = {
    button: {
        width:   '100%',
        display: 'block',
    },
    card: {
        maxWidth: 345,
    },
    media: {
        height:     0,
        paddingTop: '80%',
        marginTop:  '-20%',
    },
    action: {
        textTransform: 'capitalize',
    },
};

const URL_TYPE_DETAIL = 'detail';

const HeroCard = ({
    id,
    name,
    urls,
    thumbnail,
    onClick,
    classes,
}) => (
    <ButtonBase onClick={() => onClick(id)} className={classes.button}>
        <Card className={classes.card}>
            <CardMedia
                className={classes.media}
                image={`${thumbnail.path}.${thumbnail.extension}`}
                title={name}
            />
            <CardContent>
                <Typography gutterBottom variant="title" component="h3">
                    {name}
                </Typography>
            </CardContent>
            <CardActions>
                {urls.map(url => (
                    <Button
                        onClick={e => e.stopPropagation()}
                        key={url.type}
                        target="_blank"
                        className={styles.action}
                        href={url.url}
                        size="small"
                        color={url.type === URL_TYPE_DETAIL ? 'secondary' : 'primary'}
                    >
                        {url.type}
                    </Button>
                ))}
            </CardActions>
        </Card>
    </ButtonBase>
);

HeroCard.propTypes = {
    id:        PropTypes.number.isRequired,
    name:      PropTypes.string.isRequired,
    urls:      PropTypes.array.isRequired,
    thumbnail: PropTypes.object.isRequired,
    onClick:   PropTypes.func.isRequired,
    classes:   PropTypes.object.isRequired,
};

export default withStyles(styles)(HeroCard);
