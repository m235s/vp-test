import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
    paper: theme.mixins.gutters({
        paddingTop:    16,
        paddingBottom: 16,
        marginTop:     theme.spacing.unit * 2,
    }),
    description: {
        paddingBottom: 32,
    },
});

const HeroDescription = ({ name, description, classes }) => (
    <div>
        <Typography gutterBottom variant="display2" component="h1">
            {name}
        </Typography>
        {description && (
            <div className={classes.description}>
                <Paper className={classes.paper} elevation={2}>
                    <Typography gutterBottom variant="subheading" component="p">
                        {description}
                    </Typography>
                </Paper>
            </div>
        )}
    </div>
);

HeroDescription.propTypes = {
    name:        PropTypes.string.isRequired,
    classes:     PropTypes.object.isRequired,
    description: PropTypes.string,
};

export default withStyles(styles)(HeroDescription);
