import { push } from 'react-router-redux';
import md5 from 'md5';
import BASE_URL, { API_PRIVATE, API_PUBLIC } from '../config';

// Some utils
const qs = params => `?${Object.keys(params)
    .map(k => `${encodeURIComponent(k)}=${encodeURIComponent(params[k])}`)
    .join('&')}`;

const buildUrl = (endPoint, params) => {
    const ts = Date.now() / 1000;
    return `${BASE_URL}/v1/public/${endPoint}${qs({
        ts,
        apikey: API_PUBLIC,
        hash:   md5(ts + API_PRIVATE + API_PUBLIC),
        ...params,
    })}`;
};

const handleResponse = (response) => {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
};

// On fetch fail
export const FAIL_TO_FETCH = 'FAIL_TO_FETCH';
export const somethingBadAppend = message => ({
    type: FAIL_TO_FETCH,
    message,
});

// Use a Promise to delay and dispatch history push after store was updated
const failToFetch = message => dispatch => new Promise((resolve, reject) => {
    try {
        dispatch(somethingBadAppend(message));
        resolve(message);
    } catch (e) {
        reject(e, message);
    }
});

export const showError = msg => dispatch => dispatch(failToFetch(msg)).then(() => dispatch(push('/error')));

// Fetch heroes
export const REQUEST_HEROES_LIST = 'REQUEST_HEROES_LIST';
export const RECEIVE_HEROES_LIST = 'RECEIVE_HEROES_LIST';

const requestHeroes = () => ({
    type: REQUEST_HEROES_LIST,
});

const receiveHeroes = json => ({
    type: RECEIVE_HEROES_LIST,
    ...json.data,
});

const fetchHeroesList = offset => (dispatch) => {
    dispatch(requestHeroes());

    return fetch(buildUrl('characters', { offset }))
        .then(handleResponse)
        .then(response => response.json())
        .then(json => dispatch(receiveHeroes(json)))
        .catch(e => dispatch(showError(e.message)));
};

const shouldFetchHeroesList = (heroes, isOnMount) => (
    (isOnMount) ? heroes.loadOnMount : !heroes.isFetching && heroes.remaining !== 0
);

export const fetchHeroesIfNeeded = isOnMount => (dispatch, getState) => {
    const { heroes } = getState();
    if (shouldFetchHeroesList(heroes, isOnMount || false)) {
        return dispatch(fetchHeroesList(heroes.offset));
    }
    return null;
};

// Update current selected hero and push page
export const SET_CURRENT_HERO = 'SET_CURRENT_HERO';
export const setCurrentHero = id => ({
    type: SET_CURRENT_HERO,
    id,
});

// Use a Promise to delay and dispatch history push after store was updated
const currentHero = id => dispatch => new Promise((resolve, reject) => {
    try {
        dispatch(setCurrentHero(id));
        resolve(id, dispatch);
    } catch (e) {
        reject(e, id);
    }
});

export const showHero = id => dispatch => dispatch(currentHero(id)).then(() => dispatch(push(`/hero/${id}`)));

// Fetch hero
export const REQUEST_HERO = 'REQUEST_HERO';
export const RECEIVE_HERO = 'RECEIVE_HERO';

const requestHero = id => ({
    type: REQUEST_HERO,
    id,
});

const receiveHero = (id, hero) => ({
    type: RECEIVE_HERO,
    id,
    hero,
});

const fetchHero = id => (dispatch) => {
    dispatch(requestHero(id));
    return fetch(buildUrl(`characters/${id}`))
        .then(handleResponse)
        .then(response => response.json())
        .then(json => dispatch(receiveHero(id, json.data.results[0])))
        .catch(e => dispatch(showError(e.message)));
};

const shouldFetchHero = (id, heroes) => !heroes.isFetching && heroes.detail[id] === undefined;

export const fetchHeroIfNotExist = id => (dispatch, getState) => {
    const { heroes } = getState();
    if (shouldFetchHero(id, heroes)) {
        return dispatch(fetchHero(id));
    }
    return null;
};
