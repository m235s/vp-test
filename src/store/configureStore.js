import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import heroesReducer from '../reducers/index';

const loggerMiddleware = createLogger();

const configureStore = history => createStore(
    combineReducers({
        heroes: heroesReducer,
        router: routerReducer,
    }),
    applyMiddleware(thunkMiddleware, routerMiddleware(history), loggerMiddleware),
);

export default configureStore;
