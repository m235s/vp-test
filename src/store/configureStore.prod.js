import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import heroesReducer from '../reducers/index';

const configureStore = history => createStore(
    combineReducers({
        heroes: heroesReducer,
        router: routerReducer,
    }),
    applyMiddleware(thunkMiddleware, routerMiddleware(history)),
);

export default configureStore;
